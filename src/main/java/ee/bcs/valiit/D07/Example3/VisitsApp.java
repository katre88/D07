package ee.bcs.valiit.D07.Example3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class VisitsApp {

    public static void main(String[] args) {
        if (args.length > 0) {
            try {
                String filePath = args[0];
                Path path = Paths.get(filePath);
                List<String> fileLines = Files.readAllLines(path);
                Visits[] visits = new Visits[fileLines.size()];
                for (int i = 0; i < fileLines.size(); i++) {
                    String[] lineParts = fileLines.get(i).split(", ");
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = format.parse(lineParts[0]);
                    int nrVisitors = Integer.parseInt(lineParts[1]);
                    Visits visit = new Visits(date, nrVisitors);
                    visits[i] = visit;
                }


                Arrays.sort(visits);

                System.out.println(String.format("Max külastuste arv oli: %1$td. %1$tB %1$tY" , Arrays.stream(visits).max(Comparator.comparingInt(Visits::getNrVisitors)).get().getDate()));


                for (Visits visit : visits) {
                    System.out.println(visit);
                }

                System.out.println("Kas on võrdsed: " + visits[0].equals(visits[1]));
                System.out.println("Kas need on ikka päriselt võrdsed: " + (visits[0].hashCode() == visits[1].hashCode()));

            } catch (IOException e) {
                System.err.println("Faili lugemine ebaõnnestus.");
            } catch (ParseException e) {
                System.err.println("Kuupäeva formaat on vale.");
            }


        } else {
            System.out.println("Faili asukoht on kohustuslik!");
        }


    }
}