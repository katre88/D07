package ee.bcs.valiit.D07.Example3;

import lombok.Data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

@Data
public class Visits implements Comparable<Visits> {

    private Date date;
    private int nrVisitors;

    public Visits(Date date, int nrVisitors) {
        this.date = date;
        this.nrVisitors = nrVisitors;
    }

    @Override
    public String toString() {
        SimpleDateFormat form = new SimpleDateFormat("dd. MMMM yyyy");
        return form.format(date) + ", " + nrVisitors;
    }

    @Override
    public int hashCode() {
        int dateHash = Math.abs((30*date.getDate() + date.getMonth()) * date.getYear());
        int nrVisitorsHash = Math.abs(256 * nrVisitors);
        return dateHash + nrVisitorsHash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } if (obj == null) {
            return false;
        } if (this.getClass() != obj.getClass()) {
            return false;
        }
        Visits otherVisits = (Visits) obj;
        boolean dateIsSame = this.date.toString().equals(otherVisits.date.toString());
        boolean nrVisitorsIsSame = this.nrVisitors == otherVisits.getNrVisitors();

        return dateIsSame && nrVisitorsIsSame;
    }


    @Override
    public int compareTo(Visits o) {
        if (nrVisitors < o.getNrVisitors()) {
            return -1;
        } else if (nrVisitors > o.getNrVisitors()) {
            return 1;
        } else {
            return 0;
        }
    }

}