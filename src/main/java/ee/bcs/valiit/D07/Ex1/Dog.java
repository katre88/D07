package ee.bcs.valiit.D07.Ex1;

import lombok.Data;


@Data
public abstract class Dog {

    private String dog;

    public Dog(String dog) {
        this.dog = dog;

    }

    protected abstract String bark();


}
