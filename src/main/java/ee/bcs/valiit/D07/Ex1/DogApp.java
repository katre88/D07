package ee.bcs.valiit.D07.Ex1;

public class DogApp {
    public static void main(String[] args) {
        DogEnglish english = new DogEnglish("Buddy");
        String bark1 = english.bark();
        System.out.println(bark1);

        DogEstonia estonia = new DogEstonia("Muki");
        String bark2 = estonia.bark();
        System.out.println(bark2);


        DogGreek greek = new DogGreek("Spiros");
        String bark3 = greek.bark();
        System.out.println(bark3);
    }
}
