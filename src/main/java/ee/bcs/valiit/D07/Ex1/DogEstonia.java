package ee.bcs.valiit.D07.Ex1;


public class DogEstonia extends Dog {


    public DogEstonia(String dog) {
        super(dog);
    }


    @Override
    public String bark() {
        String text = "auh-auh";
        StringBuilder barking = new StringBuilder(getDog()).append(" says \"" + text + "\".");
        return barking.toString();
    }

}
