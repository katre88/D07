package ee.bcs.valiit.D07.Ex1;

public class DogEnglish extends Dog {

    public DogEnglish(String dog) {
        super(dog);
    }


    @Override
    public String bark() {
        String text = "woof-woof, ruff-ruff, arf-arf, bow-wow";
        StringBuilder barking = new StringBuilder(getDog()).append(" says \"" + text + "\".");
        return barking.toString();
    }

}
