package ee.bcs.valiit.D07.Ex1;

public class DogGreek extends Dog {

    public DogGreek(String dog) {
        super(dog);
    }


    @Override
    public String bark() {
        String text = "ghav-ghav";
        StringBuilder barking = new StringBuilder(getDog()).append(" says \"" + text + "\".");
        return barking.toString();
    }
}
