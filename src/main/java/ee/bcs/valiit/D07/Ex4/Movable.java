package ee.bcs.valiit.D07.Ex4;

public interface Movable {

    String getColor();
    void setColor(String color);

    int getMaxSpeed();
    void setMaxSpeed(int speed);

    void drive();

}
