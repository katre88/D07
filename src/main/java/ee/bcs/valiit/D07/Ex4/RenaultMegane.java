package ee.bcs.valiit.D07.Ex4;

public class RenaultMegane extends Car {

    @Override
    protected String blinkLights() {
        return "Blinking once";
    }

    @Override
    public void drive() {

    }

}
