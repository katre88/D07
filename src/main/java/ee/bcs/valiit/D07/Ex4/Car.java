package ee.bcs.valiit.D07.Ex4;

import lombok.Data;

@Data
public abstract class Car implements Movable {


    private String brand;
    private String engine;
    private String color;
    private int maxSpeed;

    protected abstract String blinkLights();

    public abstract void drive();

}
