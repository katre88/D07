package ee.bcs.valiit.D07.Ex4;

import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class CarApp {

    public static void main(String[] args) {


        RenaultMegane megane2 = new RenaultMegane();
        megane2.setColor("black");
        megane2.setMaxSpeed(190);
        megane2.setEngine("diesel");
        megane2.setBrand("Renault Megane");

        RenaultMegane megane1 = new RenaultMegane();
        megane1.setColor("white");
        megane1.setMaxSpeed(190);
        megane1.setEngine("diesel");
        megane1.setBrand("Renault Megane");

        FerrariF355 ferrari1 = new FerrariF355();
        ferrari1.setColor("red");
        ferrari1.setMaxSpeed(240);
        ferrari1.setEngine("gas");
        ferrari1.setBrand("Ferrari F355");

        FordFiesta fiesta1 = new FordFiesta();
        fiesta1.setColor("green");
        fiesta1.setMaxSpeed(185);
        fiesta1.setEngine("diesel");
        fiesta1.setBrand("Ford Fiesta");

        OpelAstra opel1 = new OpelAstra();
        opel1.setBrand("Opel Astra");



        List<Car> cars = new ArrayList<>();
        cars.add(megane2);
        cars.add(ferrari1);
        cars.add(fiesta1);
        cars.add(megane1);
        cars.add(opel1);

        for (Car car : cars) {
            if (car instanceof RenaultMegane) {
                StringBuilder megans = new StringBuilder(car.getBrand()).append("\n \tColor is: ").append(car.getColor()).append("\n \t" + car.blinkLights());
                System.out.println(megans.toString());
            } else if (car instanceof FerrariF355) {
                    StringBuilder ferraris = new StringBuilder(car.getBrand()).append("\n \tMax speed is: ").append(car.getMaxSpeed()).append("\n \t" + car.blinkLights());
                    System.out.println(ferraris.toString());
            } else if (car instanceof FordFiesta) {
                StringBuilder fords = new StringBuilder(car.getBrand()).append("\n \tIts engine is: ").append(car.getEngine()).append("\n \t" + car.blinkLights());
                System.out.println(fords.toString());
                System.out.print("\t");
                car.drive();
            } else {
                System.out.println(car.getBrand() + " is an unknown car");
            }
        }


    }

}
