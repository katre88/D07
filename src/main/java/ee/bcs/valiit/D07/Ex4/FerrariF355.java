package ee.bcs.valiit.D07.Ex4;

public class FerrariF355 extends Car {

    @Override
    protected String blinkLights() {
        return "Blinking continuously";
    }

    @Override
    public void drive() {

    }

}

