package ee.bcs.valiit.D07.Ex4;

public class FordFiesta extends Car {
    @Override
    protected String blinkLights() {
        return "Never blinking";
    }

    @Override
    public void drive() {
        System.out.println("Driving slowly");

    }

}
