package ee.bcs.valiit.D07.ExampleCrypto;

import java.math.BigInteger;

public class IdCode {

    public static int deriveBirthYear(String isik) {
        char sugu = isik.charAt(0);
        StringBuilder aasta = new StringBuilder();
        switch (sugu) {
            case '1':
            case '2':
                aasta.append("18");
                break;
            case '3':
            case '4':
                aasta.append("19");
                break;
            case '5':
            case '6':
                aasta.append("20");
                break;
            default:
                break;
        }
        String kumnend = isik.substring(1, 3);
        aasta.append(kumnend);
        int resultaat = 1;
        try {
            resultaat = Integer.parseInt(aasta.toString());
        } catch (Exception e) {
            System.err.println("Kuule, Su sisestatud isikukood on jura!");
        }
        return resultaat;
    }

    public static boolean isCheckNumberCorrect(BigInteger personalCode) {
        if (personalCode != null) {
            String personalCodeStr = personalCode.toString();
            if (personalCodeStr.length() == 11) {
                int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
                int checkNumber = calculateCheckNumber(personalCodeStr, weights1);
                String personalCodeLastNumberStr = personalCodeStr.substring(10, 11);
                if (checkNumber != 10) {
                    int personalCodeLastNumber = Integer.parseInt(personalCodeLastNumberStr);
                    return personalCodeLastNumber == checkNumber;
                } else {
                    int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
                    checkNumber = calculateCheckNumber(personalCodeStr, weights2);
                    checkNumber = (checkNumber == 10) ? 0 : checkNumber;
                    int personalCodeLastNumber = Integer.parseInt(personalCodeLastNumberStr);
                    return personalCodeLastNumber == checkNumber;
                }
            }
        }
        return false;
    }

    private static int calculateCheckNumber(String personalCode, int[] weights) {
        int tmpSum = 0;
        for (int i = 0; i < weights.length; i++) {
            tmpSum += Integer.parseInt(personalCode.substring(i, i + 1)) * weights[i];
        }
        return tmpSum % 11;
    }

    public static void main(String[] args) {
        String personalCode = "48806090310";
        int resultaat = deriveBirthYear(personalCode);
        System.out.println(resultaat);
        boolean isCorrect = isCheckNumberCorrect(new BigInteger(personalCode));
        System.out.println("Isikukoodi kontrollsumma oli " + (isCorrect ? "korrektne" : "vale"));
    }

}
