package ee.bcs.valiit.D07.Ex2;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DateBasedEncryptor extends Cryptor {
    public DateBasedEncryptor(String filePath) throws IOException {
        super(filePath);
    }

    @Override
    protected Map<String, String> generateDictionary(List<String> fileLines) {
        Map<String, String> dictionary = new HashMap<>();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date = cal.get(Calendar.DATE);
        for (String dictLine : fileLines) {
            char c = dictLine.charAt(0);
            StringBuilder key = new StringBuilder(Integer.toString(c + year + month + date))
                    .append(";");
            dictionary.put(dictLine, key.toString());
        }
        return dictionary;
    }

    @Override
    public String translate(String text) {
        StringBuilder result = new StringBuilder();
        if (text != null) {

            char[] textChars = text.toCharArray();
            for (char c : textChars) {
                String letter = String.valueOf(c).toUpperCase();
                String sym = dictionary.get(letter);
                result.append(sym != null ? this.dictionary.get(letter) : letter);
            }

        }


        return result.toString();
    }

}

