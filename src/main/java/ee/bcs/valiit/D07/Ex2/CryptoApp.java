package ee.bcs.valiit.D07.Ex2;

import ee.bcs.valiit.D07.ExampleCrypto.Decryptor;
import ee.bcs.valiit.D07.ExampleCrypto.Encryptor;

import java.io.IOException;
import java.util.Calendar;

public class CryptoApp {
    public static void main(String[] args) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date = cal.get(Calendar.DATE);

        String envelope = "";
        //Mati:
        try {
            DateBasedEncryptor encryptor = new DateBasedEncryptor("C:\\Users\\opilane\\alfabeet.txt");
            String encText = encryptor.translate("Võti on mati all");
            System.out.println("ENC: " + encText);
            envelope = encText;
        } catch (IOException e) {
            System.out.println("Võtmefaili ei suutnud lugeda");
        }

        //Kati loeb:
        try {
            DateBasedDecryptor decryptor = new DateBasedDecryptor("C:\\Users\\opilane\\alfabeet.txt");
            String decText = decryptor.translate(envelope);
            System.out.println("DEC: " + decText);
        } catch (IOException e) {
            System.out.println("Võtmefaili ei suutnud lugeda");
        }

    }
}
