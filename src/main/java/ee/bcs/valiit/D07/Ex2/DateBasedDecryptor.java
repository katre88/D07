package ee.bcs.valiit.D07.Ex2;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;


public class DateBasedDecryptor extends Cryptor {
    public DateBasedDecryptor(String filePath) throws IOException {
        super(filePath);
    }

    @Override
    protected Map<String, String> generateDictionary(List<String> fileLines) {
        return null;
    }

    @Override
    public String translate(String text) {
        StringBuilder result = new StringBuilder();
        if (text != null) {
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int date = cal.get(Calendar.DATE);
            String[] textCode = text.split(";");
            for (String code : textCode) {
                int sym = Integer.parseInt(code) - year - month - date;
                char sym2 = (char) sym;
                String sym3 = Character.toString(sym2);
                result.append(sym3);
            }
        }
        return result.toString();

    }
}
