package ee.bcs.valiit.D07.ExampleCrypto;

import java.io.IOException;


public class CryptoApp {

    public static void main(String[] args) {

        String envelope = "";
        //Mati:
        try {
            Encryptor encryptor = new Encryptor("C:\\Users\\opilane\\IdeaProjects\\D07\\src\\main\\resources\\alfabeet2.txt");
            String encText = encryptor.translate("Võti on mati all !?%");
            System.out.println("ENC: " + encText);
            envelope = encText;
        }catch (IOException e) {
            System.out.println("Võtmefaili ei suutnud lugeda");
        }

        //Kati loeb:
        try {
            Decryptor decryptor = new Decryptor("C:\\Users\\opilane\\IdeaProjects\\D07\\src\\main\\resources\\alfabeet2.txt");
            String decText = decryptor.translate(envelope);
            System.out.println("DEC: " + decText);
        }catch (IOException e) {
            System.out.println("Võtmefaili ei suutnud lugeda");
        }

    }
}
