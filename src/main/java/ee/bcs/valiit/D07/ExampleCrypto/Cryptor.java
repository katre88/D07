package ee.bcs.valiit.D07.ExampleCrypto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public abstract class Cryptor {

    //sisendiks map'i defineerimine
    protected Map<String, String> dictionary = null;

    //sisendi defineerimine, errorid võivad tekkida - teeme exceptioni
    public Cryptor(String filePath) throws IOException {
        List<String> fileLines = readAlphabet(filePath);
        this.dictionary = generateDictionary(fileLines);

    }

    //Fail objektina, loe faili ridu
    protected List<String> readAlphabet(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        return Files.readAllLines(path);

    }

    //abstraktsetel meetoditel pole sisu; see meetod teeb võtmebaasi valmis
    protected abstract Map<String, String> generateDictionary(List<String> fileLines);

    //Krüpteerib ja dekrüpteerib
    public String translate(String text) {
        if (text != null) {
            StringBuilder result = new StringBuilder();
            char[] textChars = text.toCharArray();
            for (char c : textChars) {
                String letter = String.valueOf(c).toUpperCase();
                result.append(this.dictionary.get(letter));
            }

            return result.toString();
        }
        return null;
    }

}
