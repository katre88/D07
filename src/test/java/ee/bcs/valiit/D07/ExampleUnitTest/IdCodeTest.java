package ee.bcs.valiit.D07.ExampleUnitTest;

import static org.junit.Assert.*;
import ee.bcs.valiit.D07.ExampleCrypto.IdCode;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

public class IdCodeTest {

    String[] personalCode = {
            "48806090310",
            "48806090311"};


    @Before
    public void setUp() {
        //personalCode = "48806090310";

    }

    @Test
    public void testCheckNumber(){
        assertTrue(IdCode.isCheckNumberCorrect(new BigInteger(personalCode[0])));
        assertFalse(IdCode.isCheckNumberCorrect(new BigInteger(personalCode[1])));
    }

    @Test
    public void testYear() {
        int year = IdCode.deriveBirthYear(personalCode[0]);
        assertEquals(year, 1988);
        assertNotEquals(year, 1970);
    }
}
