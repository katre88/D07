package ee.bcs.valiit.D07.Ex2;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Calendar;

import static org.junit.Assert.*;

public class Ex3 {
    String encoder = null;
    String decoder = null;


    Calendar cal = Calendar.getInstance();
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH);
    int date = cal.get(Calendar.DATE);


    @Before
    public void setUp() {
        encoder = "test";
        char[] textChars = encoder.toUpperCase().toCharArray();
        StringBuilder key = new StringBuilder();
        for (char c : textChars) {
            key.append(Integer.toString(c + year + month + date)).append(";");
            decoder = key.toString();
        }

    }

    @Test
    public void translateEncryptor() throws IOException {
        DateBasedEncryptor encryptor = new DateBasedEncryptor("C:\\Users\\opilane\\alfabeet.txt");
        String coded = encryptor.translate(encoder);
        assertTrue(decoder.equals(coded));

    }

    @Test
    public void translateDecryptor() throws IOException {
        DateBasedDecryptor decryptor = new DateBasedDecryptor("C:\\Users\\opilane\\alfabeet.txt");
        String decoded = decryptor.translate(decoder);
        assertTrue(decoded.equals(encoder.toUpperCase()));

    }


}
